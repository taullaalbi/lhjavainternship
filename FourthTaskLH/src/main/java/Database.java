import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class Database {
    private Connection con;
    private Properties connectionProbs = new Properties();

    public Database(String url, String user, String password) throws SQLException{
        connectionProbs.put("user", user); //insert USER here
        connectionProbs.put("password", password); //insert PASSWORD here
        this.con = DriverManager.getConnection(url, connectionProbs);
    }

    public Connection getConnection() {
        return this.con;
    }

    public Statement getStatement() throws SQLException{
        return this.con.createStatement();
    }

    public void closeConnection() throws SQLException{
        this.con.close();
    }
}