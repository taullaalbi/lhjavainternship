import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {
    public static void main(String[] args) {
        try {
            Database db = new Database("jdbc:mysql://localhost:3306/company", "albi", "Frenkli1");
            // Statement created through connection so its related to the db we are connected
            Statement stmnt = db.getStatement();


            stmnt.execute("CREATE TABLE IF NOT EXISTS employees(" +
                    "employee_no INT AUTO_INCREMENT PRIMARY KEY," +
                    "birth_date DATE NOT NULL," +
                    "first_name VARCHAR(100) NOT NULL," +
                    "last_name VARCHAR(100) NOT NULL," +
                    "gender VARCHAR(1) NOT NULL," +
                    "hire_date DATE NOT NULL" +
                    ");");

            stmnt.execute("CREATE TABLE IF NOT EXISTS salaries(" +
                    "emp_no INT NOT NULL," +
                    "salary INT NOT NULL," +
                    "from_date DATE NOT NULL," +
                    "to_date DATE NOT NULL," +
                    "CONSTRAINT pk PRIMARY KEY (emp_no, from_date)," +
                    "CONSTRAINT emp_fk FOREIGN KEY (emp_no) REFERENCES employees(employee_no)" +
                    ");");

            stmnt.execute("CREATE TABLE IF NOT EXISTS titles(" +
                    "emp_no INT NOT NULL," +
                    "title VARCHAR(255) NOT NULL," +
                    "from_date DATE NOT NULL," +
                    "to_date DATE NOT NULL," +
                    "CONSTRAINT pk PRIMARY KEY (emp_no,title, from_date)," +
                    "CONSTRAINT title_emp_fk FOREIGN KEY (emp_no) REFERENCES employees(employee_no)" +
                    ");");

            stmnt.execute("CREATE TABLE IF NOT EXISTS departments(" +
                    "dept_no INT NOT NULL," +
                    "dept_name VARCHAR(255) NOT NULL," +
                    "CONSTRAINT dept_pk PRIMARY KEY (dept_no)" +
                    ");");

            stmnt.execute("CREATE TABLE IF NOT EXISTS dept_manager(" +
                    "dept_no INT NOT NULL," +
                    "emp_no INT NOT NULL," +
                    "from_date DATE NOT NULL," +
                    "to_date DATE NOT NULL," +
                    "CONSTRAINT dept_manager_pk PRIMARY KEY (dept_no,emp_no)," +
                    "CONSTRAINT dept_manager_emp_fk FOREIGN KEY (emp_no) REFERENCES employees(employee_no)," +
                    "CONSTRAINT dept_manager_dep_fk FOREIGN KEY (dept_no) REFERENCES departments(dept_no)" +
                    ");");

            stmnt.execute("CREATE TABLE IF NOT EXISTS dept_emp(" +
                    "emp_no INT NOT NULL," +
                    "dept_no INT NOT NULL," +
                    "from_date DATE NOT NULL," +
                    "to_date DATE NOT NULL," +
                    "CONSTRAINT dept_emp_pk PRIMARY KEY (dept_no,emp_no)," +
                    "CONSTRAINT dept_emp_emp_fk FOREIGN KEY (emp_no) REFERENCES employees(employee_no)," +
                    "CONSTRAINT dept_emp_dep_fk FOREIGN KEY (dept_no) REFERENCES departments(dept_no)" +
                    ");");


            String sql = "SELECT E.employee_no, DE.from_date, DE.to_date " +
                    "FROM employees AS E INNER JOIN dept_emp AS DE ON E.employee_no=DE.emp_no " +
                    "INNER JOIN departments D ON DE.dept_no = D.dept_no";
            ResultSet rs = stmnt.executeQuery(sql);

            while(rs.next()) {
                System.out.println(rs.getInt("E.emp_no") + " " +
                        rs.getString("DE.from_date") + " " +
                        rs.getString("DE.to_date"));
            }

            // Order important
            stmnt.close();
            db.closeConnection();

        } catch (SQLException e) {
            System.out.println("Something went wrong: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
        }
    }
}
