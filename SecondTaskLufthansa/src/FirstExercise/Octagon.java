package FirstExercise;


public class Octagon extends GeometricObject implements Comparable<Octagon>,Cloneable{
    public double side;

    public Octagon(double side) {
        this.side = side;
    }

    public double getPerimeter() {
        return 8 * this.side;
    }

    public double getArea() {
        return (2+4/22) * side * side;
    }

    @Override
    public int compareTo(Octagon o) {
        if(this.getArea() > o.getArea())
            return 1;
        else if(this.getArea() < o.getArea())
            return -1;
        else return 0;
    }

    @Override
    public Object clone() throws CloneNotSupportedException{
        Octagon octagonClone = (Octagon) super.clone();
        octagonClone.dateCreated = (java.util.Date) dateCreated.clone();

        return octagonClone;
    }
}
