package FirstExercise;

public class OctagonTest {
    public static void main(String[] args) {
        try {
            Octagon o1 = new Octagon(5);
            Octagon o2 = (Octagon)o1.clone();
            String message = "";
            if(o1.compareTo(o2) == 1)
                System.out.println("Octagon 1 has greater area than octagon 2.");
            else if(o1.compareTo(o2) == -1)
                System.out.println("Octagon 1 has smaller area than octagon 2.");
            else System.out.println("The octagons have equal areas.");
        }
        catch(CloneNotSupportedException e) {
            System.out.println(e.getMessage());
        }
    }
}
