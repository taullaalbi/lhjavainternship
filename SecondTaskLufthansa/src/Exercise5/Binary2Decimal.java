package Exercise5;

public class Binary2Decimal {
    public static void main(String[] args) {
        try{
            System.out.println(bin2Dec("1004.01"));
        }
        catch(NumberFormatException e) {
            System.out.println("Wrong format exception!");
        }
    }

    public static double bin2Dec(String binaryString) throws NumberFormatException {
        double result = 0.0;
        String[] arrOfStr = binaryString.split("[.]");


        StringBuilder decimal = new StringBuilder(arrOfStr[0]);
        decimal = decimal.reverse();

        for(int i = 0; i < decimal.length(); i++) {
            if(decimal.charAt(i) != '0' && decimal.charAt(i) != '1')
                throw new NumberFormatException();

            int digit = (int)(decimal.charAt(i) - '0');
            result += digit * Math.pow(2, i);
        }

        if(arrOfStr.length > 1) {
            String fractional = arrOfStr[1];
            for(int i = 0 ;  i < fractional.length() ; i++) {
                if(fractional.charAt(i) != '0' && fractional.charAt(i) != '1')
                    throw new NumberFormatException();

                int digit = (int)(fractional.charAt(i) - '0');
                result += digit * Math.pow(2, -(i+1));
            }
        }

        return result;
    }
}
