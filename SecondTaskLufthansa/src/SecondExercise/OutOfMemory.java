package SecondExercise;

import java.util.Arrays;

public class OutOfMemory {
    public static void main(String[] args) {
        try{
            int[] arr = new int[1000];
            while(true) {
                arr = Arrays.copyOf(arr, arr.length*2);
            }
        }
        catch(OutOfMemoryError err) {
            System.out.println("Out of memory error");
        }
    }
}
