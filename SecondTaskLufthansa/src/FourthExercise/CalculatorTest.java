package FourthExercise;

import java.util.Scanner;

public class CalculatorTest {
    public static void main(String[] args) {
        try {
            Scanner in = new Scanner(System.in);
            System.out.print("Enter expression: ");
            String expression = in.nextLine();

            Calculator calc = new Calculator(expression);
            System.out.println("Result: " + calc.evaluateExpression());
        }
        catch(NumberFormatException e) {
            System.out.println(e.getMessage() + " Make sure the operands are numbers!");
        }
        catch(IllegalArgumentException e) {
            System.out.println("Expression must not be empty!");
        }
    }
}
