package FourthExercise;

import java.util.Stack;

public class Calculator {
    String expression;

    public Calculator(String expression) {
        this.expression = expression;
    }

    public int evaluateExpression()
            throws IllegalArgumentException, NumberFormatException{
        if(expression.isEmpty())
            throw new IllegalArgumentException("Wrong expression format! Try again!");

        Stack<Character> operators = new Stack<Character>();
        Stack<Integer> operands = new Stack<Integer>();

        // Ndarja ne tokens
        expression = insertSpaces(expression);
        String[] tokens = expression.split(" ");

        for(String token: tokens){
            if(token.isEmpty()) {
                continue;
            }
            else if(token.charAt(0) == '^'){
                while(!operators.isEmpty() &&
                        operators.peek() == '^')
                    processOperator(operators,operands);

                operators.push(token.charAt(0));
            }
            else if(token.charAt(0) == '+' || token.charAt(0) == '-'){
                while(!operators.isEmpty() &&
                        (operators.peek() == '+' ||
                                operators.peek() == '-' ||
                                operators.peek() == '*' ||
                                operators.peek() == '/' ||
                                operators.peek() == '%'||
                                operators.peek() == '^'))
                    processOperator(operators,operands);

                operators.push(token.charAt(0));
            }
            else if(token.charAt(0) == '*' || token.charAt(0) == '/' || token.charAt(0) == '%'){
                while(!operators.isEmpty() &&
                        (operators.peek() == '*'
                                ||operators.peek() == '/' ||
                                operators.peek() == '%'||
                                operators.peek() == '^'))
                    processOperator(operators,operands);

                operators.push(token.charAt(0));
            }
            else if(token.trim().charAt(0) == '(')
                operators.push('(');
            else if(token.trim().charAt(0) == ')'){
                while(operators.peek() != '(')
                    processOperator(operators,operands);
                operators.pop();
            }
            else {
                operands.push(new Integer(token));
            }
        }
        while(!operators.isEmpty())
            processOperator(operators,operands);

        return operands.pop();
    }

    private void processOperator(Stack<Character> operators, Stack<Integer> operands){
        char operator= operators.pop();
        int firstOp = operands.pop();
        int secondOp = operands.pop();

        int result = 0;

        if(operator == '^')
            result =(int)Math.pow(secondOp,firstOp);
        else if(operator == '+')
            result = secondOp + firstOp;
        else if(operator == '-')
            result = secondOp-firstOp;
        else if(operator == '*')
            result = secondOp * firstOp;
        else if(operator == '%')
            result = secondOp%firstOp;
        else if(operator == '/')
            result = secondOp/firstOp;

        operands.push(result);
    }


    private String insertSpaces(String s){
        String result = "";

        for(int i = 0; i < s.length();i++){
            if(s.charAt(i) == '+' || s.charAt(i) == '-'
                    || s.charAt(i) == '/' || s.charAt(i) == '*' || s.charAt(i) == '%' ||
                    s.charAt(i) == '^' || s.charAt(i) == '(' ||
                    s.charAt(i) == ')' ){
                result += " " + s.charAt(i) + " ";
            }
            else result += s.charAt(i);
        }
        return result;
    }

}
