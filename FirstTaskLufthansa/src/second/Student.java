package second;

public class Student extends Person{
    public static enum Status { FRESHMAN, SOPHMORE, JUNIOR, SENIOR };
    public final Status CLASS_STATUS;

    public Student(Status CLASS_STATUS) {
        this.CLASS_STATUS = CLASS_STATUS;
    }

    public Student(String name, String address, String phoneNumber, String emailAddress, Status CLASS_STATUS) {
        super(name, address, phoneNumber, emailAddress);
        this.CLASS_STATUS = CLASS_STATUS;
    }

    @Override
    public String toString() {
        String hold = super.cutName(super.toString());
        return "Student{" + hold +
                '}';
    }
}
