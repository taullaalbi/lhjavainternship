package second;
import java.util.Date;

public class Employee extends Person{
    private String office;
    private double salary;
    private Date date;

    public Employee() {
        this("", 0.0, new Date());

    }

    public Employee(String office, double salary, Date date) {
        this.office = office;
        this.salary = salary;
        this.date = date;
    }

    public Employee(String name, String address, String phoneNumber, String emailAddress, String office, double salary, Date date) {
        super(name, address, phoneNumber, emailAddress);
        this.office = office;
        this.salary = salary;
        this.date = date;
    }

    @Override
    public String toString() {
        String hold = super.cutName(super.toString());
        return "Employee{" + hold +
                '}';
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


}
