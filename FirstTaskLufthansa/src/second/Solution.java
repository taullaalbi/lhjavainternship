package second;
import java.util.Date;

public class Solution {
    public static void main(String[] args) {
        Person p = new Person("Albi", "Street Brooklyn ES401",
                "+355693525536", "albitaulla@yahoo.com");
        Student s = new Student("James", "Street Brooklyn ES401",
                "+355693525536", "albitaulla@yahoo.com",Student.Status.FRESHMAN);
        Employee e = new Employee("Jason", "Street Brooklyn ES401",
                "+355693525536", "albitaulla@yahoo.com", "Kennedy Hall, Office 5", 12300.34, new Date());
        Faculty f = new Faculty("Molly", "Street Brooklyn ES401",
                "+355693525536", "albitaulla@yahoo.com", "Kennedy Hall, Office 5", 12300.34, new Date(), 23,3);
        Staff st = new Staff("Eren", "Street Brooklyn ES401",
                "+355693525536", "albitaulla@yahoo.com", "Kennedy Hall, Office 5", 12300.34, new Date(), "Library manager");
        System.out.println(p);
        System.out.println(s);
        System.out.println(e);
        System.out.println(f);
        System.out.println(st);
    }
}
