package second;

import java.util.Date;

public class Staff extends Employee{
    private String title;

    public Staff(String title) {
        this.title = title;
    }

    public Staff(String office, double salary, Date date, String title) {
        super(office, salary, date);
        this.title = title;
    }

    public Staff(String name, String address, String phoneNumber, String emailAddress, String office, double salary, Date date, String title) {
        super(name, address, phoneNumber, emailAddress, office, salary, date);
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        String hold = super.cutName(super.toString());

        return "Staff{" + hold +
                '}';
    }
}
