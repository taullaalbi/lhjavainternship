package second;

import java.util.Date;

public class Faculty extends Employee{
    private int officeHours;
    private int rank;

    public Faculty(int officeHours, int rank) {
        this.officeHours = officeHours;
        this.rank = rank;
    }

    public Faculty(String office, double salary, Date date, int officeHours, int rank) {
        super(office, salary, date);
        this.officeHours = officeHours;
        this.rank = rank;
    }

    public Faculty(String name, String address, String phoneNumber, String emailAddress, String office, double salary, Date date, int officeHours, int rank) {
        super(name, address, phoneNumber, emailAddress, office, salary, date);
        this.officeHours = officeHours;
        this.rank = rank;
    }

    public int getOfficeHours() {
        return officeHours;
    }

    public void setOfficeHours(int officeHours) {
        this.officeHours = officeHours;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @Override
    public String toString() {
        String hold = super.cutName(super.toString());

        return "Faculty{" + hold +
                '}';
    }
}
