package first;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        try {
            Scanner in = new Scanner(System.in);

            System.out.print("Enter balance and interest rate (e.g., 3 for 3%): ");
            double investment = Double.parseDouble(in.next());
            double rate = Double.parseDouble(in.next());

            System.out.printf("%s%.4f\n","The interest is " ,Financial.calculateInterest(investment,rate));

            System.out.print("Enter investment amount: ");
            investment = in.nextDouble();
            System.out.print("Enter annual interest rate in percentage: ");
            rate = in.nextDouble();
            System.out.print("Enter number of years: ");
            int noOfYears = in.nextInt();
            System.out.printf("%s%.2f", "Accumulated value is ", Financial.calculateFutureInvestment(investment, rate, noOfYears));
        }
        catch(NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }
}
