package first;
import java.lang.NumberFormatException;

public class Financial {
    public static double calculateInterest(double balance, double annualInterestRate) {
        double interest = balance * (annualInterestRate/1200);
        return interest;
    }

    public static double calculateFutureInvestment(double investmentAmount, double annualInterestRate,
                                                   int noOfYears) throws NumberFormatException{
        double monthlyInterest = annualInterestRate/1200;
        double futureInvestmentValue = investmentAmount * Math.pow((1 + monthlyInterest),noOfYears*12);
        return futureInvestmentValue;
    }
 }
