package com.hibernate.company.service;

import com.hibernate.company.entities.Title;
import com.hibernate.company.repository.TitleRepository;
import com.hibernate.company.repository.TitleRepositoryImpl;

import java.util.List;

public class TitleServiceImpl implements TitleService {

    static TitleRepository employeeRepository = new TitleRepositoryImpl();
    @Override
    public Title addTitle(Title e) {
        return employeeRepository.add(e);
    }

    @Override
    public void deleteTitle(String salary, Integer id) {
        employeeRepository.delete(salary,id);
    }

    @Override
    public Title findById(String salary, Integer id) {
        return employeeRepository.findById(salary, id);
    }

    @Override
    public List<Title> findAll() {
        return employeeRepository.findAll();
    }
}
