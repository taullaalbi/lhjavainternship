package com.hibernate.company.service;

import com.hibernate.company.entities.Department;

import java.util.List;

public interface DepartmentService {
    Department addDepartment(Department d);

    void deleteDepartment(int id);

    Department findById(int id);

    List<Department> findAll();
}
