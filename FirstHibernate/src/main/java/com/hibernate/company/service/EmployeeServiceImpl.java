package com.hibernate.company.service;

import com.hibernate.company.entities.Employee;
import com.hibernate.company.repository.EmployeeRepository;
import com.hibernate.company.repository.EmployeeRepositoryImpl;

import java.util.List;

public class EmployeeServiceImpl implements EmployeeService {
    static EmployeeRepository employeeRepository = new EmployeeRepositoryImpl();
    @Override
    public Employee addEmployee(Employee e) {
        return employeeRepository.add(e);
    }

    @Override
    public void deleteEmployee(int id) {
        employeeRepository.delete(id);
    }

    @Override
    public Employee findById(int id) {
        return employeeRepository.findById(id);
    }

    @Override
    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    @Override
    public List<Employee> findByDepartment(int id) {
        return employeeRepository.findByDepartmentId(id);
    }
}
