package com.hibernate.company.service;

import com.hibernate.company.entities.Salary;
import com.hibernate.company.repository.SalaryRepository;
import com.hibernate.company.repository.SalaryRepositoryImpl;

import java.util.List;

public class SalaryServiceImpl implements SalaryService {
    static SalaryRepository employeeRepository = new SalaryRepositoryImpl();
    @Override
    public Salary addSalary(Salary e) {
        return employeeRepository.add(e);
    }

    @Override
    public void deleteSalary(Double salary, Integer id) {
        employeeRepository.delete(salary,id);
    }

    @Override
    public Salary findById(Double salary, Integer id) {
        return employeeRepository.findById(salary, id);
    }

    @Override
    public List<Salary> findAll() {
        return employeeRepository.findAll();
    }

}