package com.hibernate.company.service;

import com.hibernate.company.entities.Title;

import java.util.List;

public interface TitleService {
    Title addTitle(Title e);

    void deleteTitle(String salary, Integer id);

    Title findById(String salary, Integer id);

    List<Title> findAll();
}
