package com.hibernate.company.service;

import com.hibernate.company.entities.Department;
import com.hibernate.company.repository.DepartmentRepository;
import com.hibernate.company.repository.DepartmentRepositoryImpl;

import java.util.List;

public class DepartmentServiceImpl implements DepartmentService {
    private static DepartmentRepository departmentRepository = new DepartmentRepositoryImpl();

    @Override
    public Department addDepartment(Department d) {
        return departmentRepository.add(d);
    }

    @Override
    public void deleteDepartment(int id) {
        departmentRepository.delete(id);
    }

    @Override
    public Department findById(int id) {
        return departmentRepository.findById(id);
    }

    @Override
    public List<Department> findAll() {
        return departmentRepository.findAll();
    }
}
