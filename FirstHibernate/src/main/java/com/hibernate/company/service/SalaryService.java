package com.hibernate.company.service;

import com.hibernate.company.entities.Salary;

import java.util.List;

public interface SalaryService {
    Salary addSalary(Salary e);

    void deleteSalary(Double salary, Integer id);

    Salary findById(Double salary, Integer id);

    List<Salary> findAll();
}
