package com.hibernate.company.service;

import com.hibernate.company.entities.Employee;

import java.util.List;

public interface EmployeeService {
    Employee addEmployee(Employee e);

    void deleteEmployee(int id);

    Employee findById(int id);

    List<Employee> findAll();

    List<Employee> findByDepartment(int id);
}
