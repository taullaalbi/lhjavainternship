package com.hibernate.company;
import java.util.Scanner;
import com.hibernate.company.entities.Department;
import com.hibernate.company.entities.Employee;
import com.hibernate.company.entities.Salary;
import com.hibernate.company.entities.Title;
import com.hibernate.company.service.DepartmentService;
import com.hibernate.company.service.DepartmentServiceImpl;
import com.hibernate.company.service.EmployeeService;
import com.hibernate.company.service.EmployeeServiceImpl;


import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        boolean isExited = false;

        Application app = new Application();


        while(!isExited) {
            System.out.println(app.showOptions());
            System.out.println("Enter option ? ");
            String option = input.next();

            try {
                switch (option) {
                    case "1":
                        app.displayEmployeesByDepartments();
                        break;
                    case "2":
                        System.out.print("Enter firstname: ");
                        String firstname = input.next();
                        System.out.print("Enter lastname: ");
                        String lastname = input.next();
                        System.out.print("Enter gender: ");
                        String gender = input.next();
                        LocalDate birthDate = app.getDateData("birthdate");
                        LocalDate hireDate = app.getDateData("hire");
                        Employee e = new Employee();
                        e.setFirstname(firstname);
                        e.setLastname(lastname);
                        e.setGender(gender);
                        e.setBirthdate(birthDate);
                        e.setHireDate(hireDate);
                        app.addUpdateEmployee(e);
                        break;
                    case "3":
                        System.out.println("Enter the ID of the employee to be deleted ? ");
                        int id = input.nextInt();
                        app.deleteEmployee(id);
                        System.out.println("Employee with id " + id + " has been deleted successfully!");
                        break;
                    case "4":
                        System.out.println("Enter id of employee you want to update: ");
                        int emp_id = input.nextInt();
                        input.nextLine();
                        Employee toUpdateEmployee = app.findEmployeeById(emp_id);

                        if(toUpdateEmployee != null) {
                            System.out.println("Enter data into the fields you" +
                                    "want to update! Else leave the fields empty(Current value is inside brackets): ");
                            System.out.println("Enter firstname(" + toUpdateEmployee.getFirstname() +"): ");
                            String newFirstName = input.nextLine();
                            if(!newFirstName.equals("")) {
                                toUpdateEmployee.setFirstname(newFirstName);
                            }

                            System.out.println("Enter lastname(" + toUpdateEmployee.getLastname() +"): ");
                            String newLastName = input.nextLine();
                            if(!newLastName.equals("")) {
                                toUpdateEmployee.setLastname(newLastName);
                            }

                            System.out.println("Enter gender(" + toUpdateEmployee.getGender() +"): ");
                            String newGender = input.nextLine();
                            if(!newGender.equals("")) {
                                toUpdateEmployee.setGender(newGender);
                            }

                            Salary s = app.getSalaryData();
                            if(s != null) {
                                toUpdateEmployee.addSalary(s);
                                s.setEmployee(toUpdateEmployee);
                            }

                            Title t = app.getTitleData();
                            if(t != null) {
                                toUpdateEmployee.addTitle(t);
                                t.setEmployee(toUpdateEmployee);
                            }

                            System.out.print("Enter id of department where you want to add employee(else leave empty) ? ");
                            String department_id = input.nextLine();
                            if(!department_id.equals("")) {
                                int departmentId = Integer.parseInt(department_id);
                                Department department = app.findDepartmentById(departmentId);
                                toUpdateEmployee.addDepartment(department);
                                department.addEmployee(toUpdateEmployee);

                            }
                            app.addUpdateEmployee(toUpdateEmployee);
                        } else {
                            System.out.println("Could not find employee with id " + emp_id+ "!");
                        }
                        break;
                    case "5":
                        System.out.println("You have exited this application.");
                        isExited = true;
                        break;
                }
            }
            catch(Exception e) {
                System.out.println("There was some error processing " +
                        "your request.\n" + e.getMessage());
            }
        }
    }
}
