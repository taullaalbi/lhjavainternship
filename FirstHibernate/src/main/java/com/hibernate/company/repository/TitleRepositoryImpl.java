package com.hibernate.company.repository;

import com.hibernate.company.entities.Title;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

public class TitleRepositoryImpl implements TitleRepository {
    static EntityManagerFactory emf = Persistence.createEntityManagerFactory("hibernate.company");
    static EntityManager em = emf.createEntityManager();

    @Override
    public List<Title> findAll() {
        return em.createQuery("FROM Title").getResultList();
    }

    @Override
    public Title add(Title t) {
        if(t.getTitle() != null && t.getEmployee() != null) {
            em.getTransaction().begin();
            Title oldDepartment = findById(t.getTitle(), t.getEmployee().getId());
        }
        else {
            em.getTransaction().begin();
            em.persist(t);
        }
        em.getTransaction().commit();
        return t;
    }

    @Override
    public Title findById(String title, Integer id) {
        String queryS = "SELECT S " +
                "FROM Title S " +
                "WHERE S.title = :title AND S.employee.id = :employee_id";
        TypedQuery<Title> query = em.createQuery(queryS, Title.class);
        query.setParameter("title", title);
        query.setParameter("employee_id", id);

        return query.getSingleResult();
    }

    @Override
    public void delete(String title, Integer id) {
        Title t = findById(title, id);

        if(t != null) {
            em.getTransaction().begin();
            em.remove(t);
            em.getTransaction().commit();
        }
    }
}
