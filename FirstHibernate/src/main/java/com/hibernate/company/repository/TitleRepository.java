package com.hibernate.company.repository;

import com.hibernate.company.entities.Title;

public interface TitleRepository extends Repository<Title>{
    Title findById(String title, Integer id);

    void delete(String title, Integer id);
}
