package com.hibernate.company.repository;

import com.hibernate.company.entities.Department;

public interface DepartmentRepository extends Repository<Department>{
    void delete(int id);

    Department findById(int id);
}
