package com.hibernate.company.repository;

import com.hibernate.company.entities.Employee;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class EmployeeRepositoryImpl implements EmployeeRepository{
    static EntityManagerFactory emf = Persistence.createEntityManagerFactory("hibernate.company");
    static EntityManager em = emf.createEntityManager();

    // Add dhe update
    public Employee add(Employee e) {
        if(e.getId() != null) {
            Employee existing_employee = findById(e.getId());
            em.getTransaction().begin();
            existing_employee.setFirstname(e.getFirstname());
            existing_employee.setLastname(e.getLastname());
            existing_employee.setBirthdate(e.getBirthdate());
            existing_employee.setGender(e.getGender());
            existing_employee.setHireDate(e.getHireDate());
            System.out.println(existing_employee.getSalaries());
        }
        else {
            em.getTransaction().begin();
            em.persist(e);
        }
        em.getTransaction().commit();
        return e;
    }

    public Employee findById(int id) {
        return em.find(Employee.class, id);
    }

    // Delete employee
    public void delete(int id) {
        Employee e = findById(id);
        if(e != null) {
            em.getTransaction().begin();
            em.remove(e);
            em.getTransaction().commit();
        }
    }

    @Override
    public List<Employee> findAll() {
        return em.createQuery("FROM Employee E", Employee.class).getResultList();
    }

    @Override
    public List<Employee> findByDepartmentId(int id) {
       // return em.createQuery("SELECT E FROM Employee E INNER JOIN " +
         // "DepartmentEmployee DE WHERE DE.department.id = " + id).getResultList();
        return em.createQuery("SELECT E FROM Employee E INNER JOIN E.departments D" +
                " WHERE D.id = " + id +" GROUP BY E").getResultList();

        // You can use subqueries on the lists that you have - 😎
        // Cannot do this inside FROM directly
        //return em.createQuery("SELECT E FROM Employee E " +
          //      "WHERE " + id + " IN (SELECT J.employee.id  FROM E.jobs J)").getResultList();


    }
}
