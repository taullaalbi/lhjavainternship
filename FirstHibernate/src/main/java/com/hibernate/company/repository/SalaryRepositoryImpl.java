package com.hibernate.company.repository;

import com.hibernate.company.entities.Employee;
import com.hibernate.company.entities.Salary;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

public class SalaryRepositoryImpl implements SalaryRepository {
    static EntityManagerFactory emf = Persistence.createEntityManagerFactory("hibernate.company");
    static EntityManager em = emf.createEntityManager();

    public List<Salary> findAll() {
        return em.createQuery("FROM Salary").getResultList();
    }

    public Salary add(Salary s) {
        if(s.getSalary() != null && s.getEmployee() != null) {
            em.getTransaction().begin();
            Salary oldDepartment = findById(s.getSalary(), s.getEmployee().getId());
        }
        else {
            em.getTransaction().begin();
            em.persist(s);
        }
        em.getTransaction().commit();
        return s;
    }

    @Override
    public Salary findById(Double salary, Integer id) {
        String queryS = "SELECT S " +
                "FROM Salary S " +
                "WHERE S.salary = :salary AND S.employee.id = :employee_id";
        TypedQuery<Salary> query = em.createQuery(queryS, Salary.class);
        query.setParameter("salary", salary);
        query.setParameter("employee_id", id);

        return query.getSingleResult();
    }

    @Override
    public void delete(Double salary, Integer id) {
        Salary s = findById(salary, id);

        if(s != null) {
            em.getTransaction().begin();
            em.remove(s);
            em.getTransaction().commit();
        }
    }
}
