package com.hibernate.company.repository;

import com.hibernate.company.entities.Salary;

public interface SalaryRepository extends Repository<Salary>{
    Salary findById(Double salary, Integer id);

    void delete(Double salary, Integer id);
}
