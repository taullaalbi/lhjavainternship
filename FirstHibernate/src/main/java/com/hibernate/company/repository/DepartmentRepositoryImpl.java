package com.hibernate.company.repository;

import com.hibernate.company.entities.Department;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class DepartmentRepositoryImpl implements DepartmentRepository {
    static EntityManagerFactory emf = Persistence.createEntityManagerFactory("hibernate.company");
    static EntityManager em = emf.createEntityManager();

    public List<Department> findAll() {
        return em.createQuery("FROM Department").getResultList();
    }

    public Department add(Department d) {
        if(d.getId() != null) {
            em.getTransaction().begin();
            Department oldDepartment = findById(d.getId());
            oldDepartment.setDepartmentName(d.getDepartmentName());
        }
        else {
            em.getTransaction().begin();
            em.persist(d);
        }
        em.getTransaction().commit();
        return d;
    }

    public void delete(int id) {
        Department d = findById(id);

        if(d != null) {
            em.getTransaction().begin();
            em.remove(d);
            em.getTransaction().commit();
        }
    }

    public Department findById(int id) {
        return em.find(Department.class, id);
    }
}
