package com.hibernate.company.repository;


import java.util.List;

// CRUD operations generic interface
public interface Repository<T> {
    T add(T e);

    List<T> findAll();
}
