package com.hibernate.company.repository;

import com.hibernate.company.entities.Employee;
import java.util.List;

public interface EmployeeRepository extends Repository<Employee> {

    void delete(int id);

    Employee findById(int id);

    List<Employee> findByDepartmentId(int id);
}
