package com.hibernate.company.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import com.hibernate.company.entities.*;

@Entity
@Table(name="titles")
public class Title implements Serializable {
    @Id
    @Column(name="title")
    private String title;

    @Column(name="from_date")
    private LocalDate from_date;

    @Column(name="to_date")
    private LocalDate to_date;

    @Id
    @ManyToOne
    @JoinColumn(name="emp_no")
    private Employee employee;

    public Title() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getFrom_date() {
        return from_date;
    }

    public void setFrom_date(LocalDate from_date) {
        this.from_date = from_date;
    }

    public LocalDate getTo_date() {
        return to_date;
    }

    public void setTo_date(LocalDate to_date) {
        this.to_date = to_date;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "Title{" +
                ", title='" + title + '\'' +
                ", from_date=" + from_date +
                ", to_date=" + to_date +
                '}';
    }

}
