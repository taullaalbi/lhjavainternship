package com.hibernate.company.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;

@Entity
@Table(name="employees")
// Make the persistence at the level of the fields
// If we set it to property it will use getters and setters
@Access(AccessType.FIELD)
public class Employee implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="employee_no")
    private Integer id;

    @Column(name="birth_date")
    private LocalDate birthdate;

    @Column(name="first_name")
    private String firstname;

    @Column(name="last_name")
    private String lastname;

    @Column(name="gender")
    private String gender;

    @Column(name="hire_date")
    private LocalDate hireDate;

    // Mapped by the field on the owning side
    // For each employee we can have many salaries thus one to many
    @OneToMany(mappedBy="employee", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private List<Salary> salaries= new ArrayList<>();

    // Mapped by the field on the owning side
    @OneToMany(mappedBy = "employee", cascade = CascadeType.PERSIST)
    private List<Title> titles = new ArrayList<>();

    @ManyToMany
    @JoinTable(name="dept_emp", joinColumns = {
            @JoinColumn(name="emp_no", referencedColumnName = "employee_no")
    }, inverseJoinColumns = {
            @JoinColumn(name="dept_no", referencedColumnName = "dept_no",
                    nullable = false)
    })
    private Set<Department> departments = new HashSet<Department>();

    @ManyToMany
    @JoinTable(name="dept_manager",
    joinColumns = {
            @JoinColumn(name="emp_no", referencedColumnName = "employee_no")
    },
    inverseJoinColumns = {
        @JoinColumn(name="dept_no", referencedColumnName = "dept_no", nullable = false)
    }
    )
    private Set<Department> managedDepartments = new HashSet<Department>();

    public Employee() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public LocalDate getHireDate() {
        return hireDate;
    }

    public void setHireDate(LocalDate hireDate) {
        this.hireDate = hireDate;
    }

    public List<Salary> getSalaries() {
        return salaries;
    }

    public void setSalaries(List<Salary> salaries) {
        this.salaries = salaries;
    }

    public void addSalary(Salary sal) {
        this.salaries.add(sal);
    }

    public Set<Department> getDepartments() {
        return Collections.unmodifiableSet(this.departments);
    }

    public void addDepartment(Department d) {
        this.departments.add(d);
    }

    public Set<Department> getManagedDepartments() {
        return Collections.unmodifiableSet(this.managedDepartments);
    }

    public void addManagedDepartment(Department d) {
        this.managedDepartments.add(d);
    }

    public List<Title> getTitles() {
        return titles;
    }

    public void setTitles(List<Title> titles) {
        this.titles = titles;
    }

    public void addTitle(Title t) {
        this.titles.add(t);
    }

    public void setDepartments(Set<Department> dept_list) {
        this.departments = dept_list;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", birthdate=" + birthdate +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", gender='" + gender + '\'' +
                ", hireDate=" + hireDate +
                '}';
    }
}
