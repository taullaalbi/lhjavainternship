package com.hibernate.company.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name="salaries")
public class Salary implements Serializable {
    @Column(name="salary")
    private Double salary;

    @Id
    @Column(name="from_date")
    private LocalDate from_date;

    @Column(name="to_date")
    private LocalDate to_date;

    // Many salaries can belong to one employee
    // The column specifying the foreign key
    @ManyToOne
    @JoinColumn(name="emp_no")
    private Employee employee;

    public Salary() {
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public LocalDate getFrom_date() {
        return from_date;
    }

    public void setFrom_date(LocalDate from_date) {
        this.from_date = from_date;
    }

    public LocalDate getTo_date() {
        return to_date;
    }

    public void setTo_date(LocalDate to_date) {
        this.to_date = to_date;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "Salary{" +
                ", salary=" + salary +
                ", from_date=" + from_date +
                ", to_date=" + to_date +
                '}';
    }
}
