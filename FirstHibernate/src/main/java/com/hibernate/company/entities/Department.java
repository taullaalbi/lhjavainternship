package com.hibernate.company.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name="DEPARTMENTS")
@Access(AccessType.FIELD)
public class Department implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="dept_no")
    private Integer id;

    @Column(name="dept_name")
    private String departmentName;

    // Mapped by the field on the owning side
    // OneToOne is similar
    @ManyToMany(mappedBy="departments")
    private Set<Employee> employees = new HashSet<Employee>();

    @ManyToMany(mappedBy="managedDepartments")
    private Set<Employee> managers = new HashSet<Employee>();


    public Department() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Set<Employee> getEmployees() {
        return Collections.unmodifiableSet(this.employees);
    }

    public void addEmployee(Employee e) {
        this.employees.add(e);
    }

    public Set<Employee> getManagers() {
        return Collections.unmodifiableSet(this.managers);
    }

    public void addManager(Employee m) {
        this.managers.add(m);
    }

    @Override
    public String toString() {
        return "Departments{" +
                "id=" + id +
                ", departmentName='" + departmentName + '\'' +
                '}';
    }
}

