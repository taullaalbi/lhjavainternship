package com.hibernate.company;

import com.hibernate.company.entities.Department;
import com.hibernate.company.entities.Employee;
import com.hibernate.company.entities.Salary;
import com.hibernate.company.entities.Title;
import com.hibernate.company.service.DepartmentService;
import com.hibernate.company.service.DepartmentServiceImpl;
import com.hibernate.company.service.EmployeeService;
import com.hibernate.company.service.EmployeeServiceImpl;

import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class Application {
    public static DepartmentService departmentService = new DepartmentServiceImpl();
    public static EmployeeService employeeService = new EmployeeServiceImpl();

    public String showOptions() {
        return String.format("--------- OPTIONS -------------\n" +
                "1. Display all employees according to departments\n" +
                "2. Add Employee\n" +
                "3. Delete Employee\n" +
                "4. Update Employee\n" +
                "5. Exit\n");
    }

    public void displayEmployeesByDepartments() {
        List<Department> departments = departmentService.findAll();
        for(Department d: departments) {
            System.out.println("\nDepartment Name: " + d.getDepartmentName() + "\n");
            String header = String.format("ID%20s%20s","First Name","Last Name");
            System.out.println(header);

            List<Employee> departmentEmployees = employeeService.findByDepartment(d.getId());
            for(Employee e: departmentEmployees) {
                String s = String.format("%2d%20s%20s", e.getId(), e.getFirstname(), e.getLastname());
                System.out.println(s);
            }
        }
    }

    public Employee addUpdateEmployee(Employee e) {
       return employeeService.addEmployee(e);
    }

    public void deleteEmployee(int id) {
        employeeService.deleteEmployee(id);
    }

    public LocalDate getDateData(String type) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter "+ type+  " year: ");
        int year = in.nextInt();
        System.out.print("Enter " + type +" month: ");
        int month = in.nextInt();
        System.out.print("Enter " + type + " day:");
        int day = in.nextInt();
        return LocalDate.of(year, month, day);
    }

    public Employee findEmployeeById(int id) {
        return employeeService.findById(id);
    }

    public Department findDepartmentById(int id) {
        return departmentService.findById(id);
    }

    public Salary getSalaryData() {
        Scanner in = new Scanner(System.in);
        System.out.print("Do you want to add a salary for this employee ? (Y/N)");
        String answer = in.next();
        if(answer.equals("Y")) {
            Salary s = new Salary();
            System.out.println("Enter salary");
            Double sal = in.nextDouble();
            System.out.println("Enter the starting date for the salary:");
            LocalDate from_date = getDateData("");
            System.out.println("Enter the ending date for the salary:");
            LocalDate to_date = getDateData("");
            s.setSalary(sal);
            s.setFrom_date(from_date);
            s.setTo_date(to_date);
            return s;
        }
        else return null;
    }

    public Title getTitleData() {
        Scanner in = new Scanner(System.in);
        System.out.print("Do you want to add a title for this employee ? (Y/N)");
        String answer = in.next();
        if(answer.equals("Y")) {
            Title t = new Title();
            System.out.println("Enter title");
            String title = in.next();
            System.out.println("Enter the starting date for the title held:");
            LocalDate from_date = getDateData("");
            System.out.println("Enter the ending date for the title held:");
            LocalDate to_date = getDateData("");
            t.setTitle(title);
            t.setFrom_date(from_date);
            t.setTo_date(to_date);
            return t;
        }
        else return null;
    }
}
