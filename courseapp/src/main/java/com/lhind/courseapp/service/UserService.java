package com.lhind.courseapp.service;

import com.lhind.courseapp.model.User;

import java.util.List;

public interface UserService {
    List<User> findAll();

    User findUser(Integer id);

    String deleteUser(Integer id);

    void save(User u);
}
