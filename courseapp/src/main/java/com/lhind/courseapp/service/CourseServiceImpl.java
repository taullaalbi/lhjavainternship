package com.lhind.courseapp.service;

import com.lhind.courseapp.model.Course;
import com.lhind.courseapp.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service(value = "courseService")
public class CourseServiceImpl implements CourseService {

    private CourseRepository courseRepository;

    @Autowired
    public CourseServiceImpl(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Override
    @Transactional
    public List<Course> findAll() {
        return this.courseRepository.findAll();
    }

    @Override
    @Transactional
    public Course findCourse(Integer id) {
        return this.courseRepository.findCourse(id);
    }

    @Override
    @Transactional
    public String deleteCourse(Integer id) {
        return this.courseRepository.deleteCourse(id);
    }

    @Override
    @Transactional
    public void save(Course c) {
        this.courseRepository.save(c);
    }
}
