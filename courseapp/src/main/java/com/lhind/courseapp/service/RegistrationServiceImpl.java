package com.lhind.courseapp.service;

import com.lhind.courseapp.model.Registration;
import com.lhind.courseapp.repository.RegistrationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service(value = "registrationService")
public class RegistrationServiceImpl implements RegistrationService {

    private RegistrationRepository registrationRepository;

    @Autowired
    public RegistrationServiceImpl(RegistrationRepository registrationRepository) {
        this.registrationRepository = registrationRepository;
    }

    @Override
    @Transactional
    public List<Registration> findAll() {
        return this.registrationRepository.findAll();
    }

    @Override
    @Transactional
    public Registration findRegistration(Integer id) {
        return this.registrationRepository.findRegistration(id);
    }

    @Override
    @Transactional
    public String deleteRegistration(Integer id) {
        return this.registrationRepository.deleteRegistration(id);
    }

    @Override
    @Transactional
    public void save(Registration r) {
        this.registrationRepository.save(r);
    }
}
