package com.lhind.courseapp.service;

import com.lhind.courseapp.model.Registration;

import javax.transaction.Transactional;
import java.util.List;

public interface RegistrationService {
    @Transactional
    List<Registration> findAll();

    @Transactional
    Registration findRegistration(Integer id);

    @Transactional
    String deleteRegistration(Integer id);

    @Transactional
    void save(Registration u);
}
