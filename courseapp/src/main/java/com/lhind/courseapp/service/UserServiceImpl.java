package com.lhind.courseapp.service;

import com.lhind.courseapp.model.User;
import com.lhind.courseapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service(value = "userService")
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public List<User> findAll() {
        return this.userRepository.findAll();
    }

    @Override
    @Transactional
    public User findUser(Integer id) {
        return this.userRepository.findUser(id);
    }

    @Override
    @Transactional
    public String deleteUser(Integer id) {
        return this.userRepository.deleteUser(id);
    }

    @Override
    @Transactional
    public void save(User u) {
        this.userRepository.save(u);
    }
}
