package com.lhind.courseapp.service;

import com.lhind.courseapp.model.Course;

import javax.transaction.Transactional;
import java.util.List;

public interface CourseService {
    @Transactional
    List<Course> findAll();

    @Transactional
    Course findCourse(Integer id);

    @Transactional
    String deleteCourse(Integer id);

    @Transactional
    void save(Course c);
}
