package com.lhind.courseapp;

import com.lhind.courseapp.controller.CourseController;
import com.lhind.courseapp.controller.RegistrationController;
import com.lhind.courseapp.controller.UserController;
import com.lhind.courseapp.model.Course;
import com.lhind.courseapp.model.Registration;
import com.lhind.courseapp.model.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class CourseappApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(CourseappApplication.class, args);

        UserController userController = context.getBean("userController", UserController.class);
//		USER CRUDS
//		User user1 = new User();
//		user1.setFirstName("James");
//		user1.setLastName("Moriarty");
//		user1.setAge(25);
//
//		User user2 = new User();
//		user2.setFirstName("Jonathan");
//		user2.setLastName("Jameson");
//		user2.setAge(17);
//
//		userController.saveUser(user1);
//		userController.saveUser(user2);

//		userController.postDeleteUser(1);
//		userController.postDeleteUser(2);

//		user1.setFirstName("John");
//		user1.setLastName("Moriarty");
//		user2.setFirstName("Armin");
//		user2.setLastName("Arlert");
//		userController.saveUser(user1);
//		userController.saveUser(user2);

//		System.out.println(userController.getUserById(3));

//		Registration CRUD + Course CRUD
        RegistrationController registrationController =
                context.getBean("registrationController", RegistrationController.class);
//
//		Registration registration1 = new Registration();
//		registration1.setName("First Registration");
//
//		Course course1 = new Course();
//		course1.setName("Biology");
//		course1.setDescription("Description of biology!");
//		course1.setRegistration(registration1);
//
//		Course course2 = new Course();
//		course2.setName("Calculus 2");
//		course2.setDescription("Description of calculus 2!");
//		course2.setRegistration(registration1);
//
//		registration1.addCourse(course1);
//		registration1.addCourse(course2);
//		registrationController.saveRegistration(registration1);

//		Registration registration2 = new Registration();
//		registration2.setName("Registartion 1");
//
//		registrationController.saveRegistration(registration2);
//
//		registration2.setName("Registartion 2");
//		registrationController.saveRegistration(registration2);

//		registrationController.postDeleteRegistration(2);

        CourseController courseController =
                context.getBean("courseController", CourseController.class);

//        Course course2 = new Course();
//        course2.setName("Linear Algebra");
//        course2.setDescription("Linear Algebra Description");
//		courseController.saveCourse(course2);
//
//		course2.setName("Numerical Analysis");
//		courseController.saveCourse(course2);

//		courseController.postDeleteCourse(4);

//		System.out.println(courseController.getCourseById(3));

//		System.out.println("-------COURSES---------");
//		for(Course c : courseController.getCourses())
//			System.out.println(c);
//		System.out.println("\n\n----------USERS----------");
//		for(User u: userController.getUsers())
//			System.out.println(u);
//		System.out.println("\n\n-----------REGISTRATIONS-----------");
//		for(Registration r: registrationController.getRegistrations())
//			System.out.println(r);

//      TESTING VIOLATIONS - USER
//      User user1 = new User();
//		user1.setFirstName("Micky");
//		user1.setLastName("Miles");
//		user1.setAge(14);
//
//		userController.saveUser(user1);

//      TESTING VIOLATIONS - REGISTRATION
//      Registration registration3 = new Registration();
//      registration3.setName("Registration3");
//      registrationController.saveRegistration(registration3);

//      TESTING VIOLATIONS - COURSE
//      Course course3 = new Course();
//      course3.setName("C234D");
//      course3.setDescription("Computer Organization And Architecture");
//      courseController.saveCourse(course3);
    }
}
