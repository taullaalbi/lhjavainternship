package com.lhind.courseapp.controller;

import com.lhind.courseapp.model.Course;
import com.lhind.courseapp.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.AssertTrue;
import java.util.List;

@Controller(value = "courseController")
public class CourseController {
    private CourseService courseService;

    @Autowired
    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    public List<Course> getCourses() {
        return courseService.findAll();
    }

    public Course getCourseById(int id) {
        return courseService.findCourse(id);
    }

    public String postDeleteCourse(int id) {
        return courseService.deleteCourse(id);
    }

    public void saveCourse(Course c) {
        try {
            courseService.save(c);
        } catch(ConstraintViolationException e) {
            System.out.println("Constraint Violation On Course: " + e.getMessage());
        }
    }
}
