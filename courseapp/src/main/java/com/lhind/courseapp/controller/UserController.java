package com.lhind.courseapp.controller;

import com.lhind.courseapp.model.User;
import com.lhind.courseapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.AssertTrue;
import java.util.List;

@Controller(value = "userController")
public class UserController {
    private UserService userService;

    @Autowired
    public UserController(UserService userService) {

        this.userService = userService;
    }

    public List<User> getUsers() {
        return userService.findAll();
    }

    public User getUserById(int id) {
        return userService.findUser(id);
    }

    public String postDeleteUser(int id) {
        return userService.deleteUser(id);
    }

    public void saveUser(User u) {
        try {
            userService.save(u);
        } catch(ConstraintViolationException e) {
            System.out.println("Constraint Violation: " + e.getMessage());
        }
    }
}