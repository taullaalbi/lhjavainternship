package com.lhind.courseapp.controller;

import com.lhind.courseapp.model.Registration;
import com.lhind.courseapp.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.AssertTrue;
import java.util.List;

@Controller(value = "registrationController")
public class RegistrationController {
    private RegistrationService registrationService;

    @Autowired
    public RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    public List<Registration> getRegistrations() {
        return registrationService.findAll();
    }

    public Registration getRegistrationById(int id) {
        return registrationService.findRegistration(id);
    }

    public String postDeleteRegistration(int id) {
        return registrationService.deleteRegistration(id);
    }

    public void saveRegistration(Registration r) {
        try {
            registrationService.save(r);
        }
        catch(ConstraintViolationException e) {
            System.out.println("Constraint Violation: " + e.getMessage());
        }
    }
}