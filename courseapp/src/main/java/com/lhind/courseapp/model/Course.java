package com.lhind.courseapp.model;

import com.lhind.courseapp.model.markers.OnCreate;
import com.lhind.courseapp.model.markers.OnUpdate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Pattern;

@Entity
@Table(name="course")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="course_id", unique = true)
    @Null(groups= {OnCreate.class})
    @NotNull(groups = {OnUpdate.class})
    private Integer id;

    @Column(name="course_name", nullable = false, length = 50)
    @Pattern(regexp="[a-zA-z]+[0-9]{1,3}")
    private String name;

    @Column(name="description")
    private String description;

    @ManyToOne
    @JoinColumn(name="registration_id")
    private Registration registration;

    public Course() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Registration getRegistration() {
        return registration;
    }

    public void setRegistration(Registration registration) {
        this.registration = registration;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
