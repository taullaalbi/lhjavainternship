package com.lhind.courseapp.model;

import com.lhind.courseapp.model.markers.OnCreate;
import com.lhind.courseapp.model.markers.OnUpdate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Entity
@Table(name="registration")
public class Registration {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="registration_id")
    @Null(groups={OnCreate.class})
    @NotNull(groups={OnUpdate.class})
    private Integer id;

    @Column(name="name")
    @Pattern(regexp="REG[1-9]+")
    private String name;

    @OneToMany(mappedBy="registration", fetch = FetchType.EAGER,cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<Course> courses= new ArrayList<Course>();

    public Registration() {
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Course> getCourses() {
        return Collections.unmodifiableList(courses);
    }

    public void addCourse(Course c) {
        this.courses.add(c);
    }

    @Override
    public String toString() {
        return "Registration{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", courses=" + courses +
                '}';
    }
}
