package com.lhind.courseapp.repository;

import com.lhind.courseapp.model.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository(value="courseRepository")
public class CourseRepositoryImpl implements CourseRepository {
    private EntityManager entityManager;

    @Autowired
    public CourseRepositoryImpl(EntityManager em) {
        this.entityManager = em;
    }

    @Override
    @Transactional
    public List<Course> findAll() {
        TypedQuery<Course> query = entityManager.createQuery("FROM Course c", Course.class);
        List<Course> courseList = query.getResultList();
        return courseList;
    }

    @Override
    @Transactional
    public Course findCourse(Integer id) {
        Course c = entityManager.find(Course.class, id);
        return c;
    }

    @Override
    @Transactional
    public String deleteCourse(Integer id) {
        Course c = findCourse(id);
        if(c != null) {
            entityManager.remove(c);
            return "Course successfully deleted: " + id;
        }

        return "";
    }

    @Override
    @Transactional
    public void save(Course c) {
        if(c.getId() == null) {
            entityManager.persist(c);
        } else {
            Course toSave = findCourse(c.getId());
            toSave.setName(c.getName());
            toSave.setDescription(c.getDescription());
            toSave.setRegistration(c.getRegistration());
            entityManager.merge(toSave);
        }
    }
}
