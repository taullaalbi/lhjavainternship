package com.lhind.courseapp.repository;

import com.lhind.courseapp.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository(value = "userRepository")
public class UserRepositoryImpl implements UserRepository {
    private EntityManager entityManager;

    @Autowired
    public UserRepositoryImpl(EntityManager em) {
        this.entityManager = em;
    }

    @Transactional
    @Override
    public List<User> findAll() {
        TypedQuery<User> query = entityManager.createQuery("FROM User u", User.class);
        List<User> userList = query.getResultList();
        return userList;
    }

    @Transactional
    @Override
    public User findUser(Integer id) {
        User u = entityManager.find(User.class, id);
        return u;
    }

    @Transactional
    @Override
    public String deleteUser(Integer id) {
        User u = findUser(id);
        if(u != null) {
            entityManager.remove(u);
            return "User successfully deleted: " + id;
        }
        return "";
    }

    @Transactional
    @Override
    public void save(User u) {
        if(u.getId() == null) {
            entityManager.persist(u);
        } else {
            User toSave = findUser(u.getId());
            toSave.setAge(u.getAge());
            toSave.setFirstName(u.getFirstName());
            toSave.setLastName(u.getLastName());
            entityManager.merge(toSave);
        }
    }
}
