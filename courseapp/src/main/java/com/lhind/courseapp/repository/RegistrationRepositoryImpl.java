package com.lhind.courseapp.repository;

import com.lhind.courseapp.model.Registration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository(value="registrationRepository")
public class RegistrationRepositoryImpl implements RegistrationRepository {
    private EntityManager entityManager;

    @Autowired
    public RegistrationRepositoryImpl(EntityManager em) {
        this.entityManager = em;
    }

    @Transactional
    @Override
    public List<Registration> findAll() {
        TypedQuery<Registration> query = entityManager.createQuery("FROM Registration r", Registration.class);
        List<Registration> registrationList = query.getResultList();
        return registrationList;
    }

    @Transactional
    @Override
    public Registration findRegistration(Integer id) {
        Registration r = entityManager.find(Registration.class, id);
        return r;
    }

    @Transactional
    @Override
    public String deleteRegistration(Integer id) {
        Registration r = findRegistration(id);
        if(r != null) {
            entityManager.remove(r);
            return "Registration successfully deleted: " + id;
        }
        return "";
    }

    @Transactional
    @Override
    public void save(Registration r) {
        if(r.getId() == null) {
            entityManager.persist(r);
        } else {
            Registration toSave = findRegistration(r.getId());
            toSave.setName(r.getName());
            entityManager.merge(toSave);
        }
    }
}
