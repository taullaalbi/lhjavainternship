package com.lhind.courseapp.repository;

import com.lhind.courseapp.model.Course;

import javax.transaction.Transactional;
import java.util.List;

public interface CourseRepository {
    List<Course> findAll();

    Course findCourse(Integer id);

    String deleteCourse(Integer id);

    void save(Course c);
}
