package com.lhind.courseapp.repository;

import com.lhind.courseapp.model.Registration;

import javax.transaction.Transactional;
import java.util.List;

public interface RegistrationRepository {
    List<Registration> findAll();

    Registration findRegistration(Integer id);

    String deleteRegistration(Integer id);

    void save(Registration r);
}
