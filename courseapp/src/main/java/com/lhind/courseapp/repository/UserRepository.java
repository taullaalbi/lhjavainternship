package com.lhind.courseapp.repository;

import com.lhind.courseapp.model.User;

import java.util.List;

public interface UserRepository {
    List<User> findAll();

    User findUser(Integer id);

    String deleteUser(Integer id);

    void save(User u);
}
